import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './components/app/app.component';
import { SocketIoService } from './services/socketIoService/socketio.service'
import { GraphdataService } from './services/graphDataService/graphdata.service';
import { SimulationPickerComponent } from './components/simulation-picker/simulation-picker.component';
import { ChartComponent } from './components/chart/chart.component';
import { PredictorComponent } from './components/predictor/predictor.component';
import { HttpService } from './services/httpService/http.service';
@NgModule({
  declarations: [
    AppComponent,
    SimulationPickerComponent,
    ChartComponent,
    PredictorComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [
    SocketIoService,
    GraphdataService,
    HttpService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
