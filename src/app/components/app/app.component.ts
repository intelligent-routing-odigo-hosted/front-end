import { Component } from '@angular/core';

import { Simulation } from '../../models/Simulation';

import { SocketIoService } from '../../services/socketIoService/socketio.service';
import { HttpService } from '../../services/httpService/http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', './pageloader.min.css']
})
export class AppComponent {
  simulation: Simulation = new Simulation([], true)
  interactionNb: number;
  zendeskCount: number;

  //Page state data
  loading = true;
  loadingMessage: string;
  uploadNeeded: boolean = false;
  retrainMessage: string = '';
  displayModal: boolean = false;
  
  constructor(private socketIoService: SocketIoService, private readonly httpService: HttpService){ 
    this.socketIoService.askAgentsStatuses()

    this.socketIoService.refreshOver().subscribe(() => {
      this.retrainMessage = 'Retraining algorithms 1/4'
      this.socketIoService.train('customer_satisfaction')
    }, err => {console.log(err)})

    this.socketIoService.onUploadOver().subscribe(() => {
      setTimeout(() => this.askZendeskCountEvery3Seconds(), 3000)
    }, err => {console.log(err)})

    this.socketIoService.onTrainingOver().subscribe((result) => {
      this.trainNextModel(result);
    })

    var currentSimulation = JSON.parse(localStorage.getItem('CurrentSimulation'))
    if(currentSimulation != null){
      this.uploadInteractions(currentSimulation)
    } else {
      this.getInteractions()
    }
  }

/*###################################################################################################
########## Interactions query
###################################################################################################*/
  getInteractions(){
    this.loadingMessage = 'Getting interactions from Zendesk'
    this.httpService.getInteractionsFromZendesk().subscribe(result => {
      console.log(result)
      if(result['interactions'] == null){
        console.log('No interactions stored on Zendesk')
        this.uploadNeeded = true;
        this.loading = false;
      } else {
        this.simulation = new Simulation(result['interactions'], true);
        this.socketIoService.askAgentsStatuses();
        setTimeout(() => {
          this.loading = false;
          this.retrainMessage = '';
        }, 1000);
      }
    }, err => console.log(err))
  }

/*###################################################################################################
########## Upload process (upload new interactions to Zendesk)
###################################################################################################*/
  
  //Shows the modal that enables you to upload interactions to Zendesk
  onUpload(event) {
    this.displayModal = true;
  }

  //Loads the file inputed
  onFileChange(event){
    var reader = new FileReader();
    reader.onload = this.onReaderLoad;
    reader.readAsText(event.target.files[0]);
  }

  //Stores the new simulation in the localStorage and reloads page
  onReaderLoad(event){
    console.log(event.target.result)
    localStorage.setItem('CurrentSimulation', event.target.result);
    location.reload()
  }

  //Uploads interactions and clears the localstorage when the page detects a simulation in the localstorage
  uploadInteractions(currentSimulation){
    this.interactionNb = currentSimulation.interactions.length;
    this.loadingMessage = 'Deleting previous interactions from Zendesk and sending new ones'
    var interactions = currentSimulation.interactions;
    this.socketIoService.uploadInteractionsToZendesk(interactions)
    this.simulation.isTrained = false;
    localStorage.clear()
  }

  //Bulk creating the interactions on Zendesk takes time, that's why where asking every 3 seconds the count of interactions, waiting
  //for it to be equal to the number of interactions we sent at upload.
  askZendeskCountEvery3Seconds(){
    if(this.interactionNb != this.zendeskCount){
      this.httpService.getZendeskCount().subscribe(result => {
        console.log(result['count'])
        this.loadingMessage = `Zendesk created ${result['count']}/${this.interactionNb} interactions successfully`
        this.zendeskCount = result['count'];
      }, err => {
        console.log(err)
      })
      setTimeout(() => this.askZendeskCountEvery3Seconds(), 3000)
    } else {
      this.loadingMessage = 'Training the customer satisfaction algorithm'
      this.socketIoService.train('customer_satisfaction')
    }
  }

/*###################################################################################################
########## Retrain process (refresh interactions and retrain)
###################################################################################################*/
  onRetrain(){
    this.socketIoService.refreshInteractions();
    this.retrainMessage = 'Refreshing interactions'
  }

  //Launches the training of the next model when the precedent is trained
  public trainNextModel(result){
    console.log('The ' + result + ' algo is over training')
    switch(result){
      case 'customer_satisfaction':
        this.retrainMessage = 'Retraining algorithms 2/4'
        this.loadingMessage = 'Training the agent satisfaction algorithm'
        this.socketIoService.train('agent_satisfaction');
        break;
      case 'agent_satisfaction':
        this.retrainMessage = 'Retraining algorithms 3/4'
        this.loadingMessage = 'Training the Agent efficiency algorithm'
        this.socketIoService.train('call_duration');
        break;
      case 'call_duration':
        this.retrainMessage = 'Retraining algorithms 4/4'
        this.loadingMessage = 'Training the business outcome algorithm'
        this.socketIoService.train('business_outcome');
        break;
      //Last model to train
      case 'business_outcome':
        this.getInteractions()
        break;
    }
  }

/*###################################################################################################
########## Utils
###################################################################################################*/
  public triggerDownload(){
    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(this.simulation));
    var dlAnchorElem = document.getElementById('downloadAnchorElem');
    dlAnchorElem.setAttribute("href", dataStr);
    dlAnchorElem.setAttribute("download", "simulation.json");
    dlAnchorElem.click();
  }

  public closeModals(){
    this.displayModal = false;
  }
}