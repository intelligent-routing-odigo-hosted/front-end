import { Component, OnInit, Input } from '@angular/core';
import { Simulation } from 'src/app/models/Simulation';
import { SocketIoService } from 'src/app/services/socketIoService/socketio.service';
import Plotly from 'plotly.js-dist';
import { GraphdataService } from 'src/app/services/graphDataService/graphdata.service';
import { HttpService } from 'src/app/services/httpService/http.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'predictor',
  templateUrl: './predictor.component.html',
  styleUrls: ['./predictor.component.css']
})
export class PredictorComponent implements OnInit {
  @Input() simulation: Simulation;

  //Customer information
  sex: number = 0;
  age: number = 18;
  salary: number = 0;
  intent: number = 0;
  year_of_arrival: number = 2019;

  //Agent information 
  agentInfo: Object;
  agentStatus: Array<number> = [0, 0, 0, 0, 0];
  
  //Targets and predictions
  targetValue: any;
  prediction: Array<Object> = []

  //Page mecanics
  predictable: boolean = false;
  displayPredictionModal: boolean = false;
  displayCallModal:boolean = false;
  stars: Array<boolean> = [false, false, false, false, false];
  displaySatisfaction: boolean = false;
  satisfactionObservable: any;
  target: string = 'customer_satisfaction';
  agent: number = 1;

  //Options
  sex_options: Array<Object> = [{id: 0, text: 'Male'}, {id: 1, text: 'Non Binary'}, {id: 2, text: 'Female'}];
  agentOptions = [
    {id: 1, value: 'Agent 1'},
    {id: 2, value: 'Agent 2'},
    {id: 3, value: 'Agent 3'},
    {id: 4, value: 'Agent 4'},
    {id: 5, value: 'Agent 5'}
  ]
  IVR_option: Array<Object> = [
    {id: 0, text: 'Credit card loss'},
    {id: 1, text: 'Open a loan'},
    {id: 2, text: 'Acquire a credit card'},
    {id: 3, text: 'Order checks'},
    {id: 4, text: 'Request an appointment'},
    {id: 5, text: 'Others'},
  ];
  targetOptions: Array<Object> = [
    {id: 'customer_satisfaction', value: 'Customer satisfaction'},
    {id: 'agent_satisfaction', value: 'Agent satisfaction'},
    {id: 'call_duration', value: 'Agent efficiency'},
    {id: 'business_outcome', value: 'Business outcome'}
  ]
  salary_options: Array<Object> = [
    {id: 0, text: 'Lower than $10.000'},
    {id: 1, text: '$10.000 to $30.000'},
    {id: 2, text: '$30.000 to $50.000'},
    {id: 3, text: '$50.000 to $100.000'},
    {id: 4, text: '$100.000 to $200.000'},
    {id: 5, text: 'More than $200.000'},
  ];

  constructor(private socketIoService: SocketIoService, private graphdataService: GraphdataService, private httpService: HttpService){}

  ngOnInit() {
    //Filling the data to avoid console errors for undefined objects
    for(var i of [1, 2, 3, 4, 5]){
      this.prediction.push({
        agent_number: i,
        customer_satisfaction: 0,
        agent_satisfaction: 0,
        call_duration: 0,
        business_outcome: 0,
        score: 0,
    })}

    //Querying data from services
    this.targetValue = this.socketIoService.targetValue;
    this.httpService.getAgentData().subscribe(agentInfo => {
      this.agentInfo = agentInfo['agent_info'];
    }, err => console.log(err))

    //Refreshes the agentStatus and looks if at least one agent is connected
    this.socketIoService.agentStatusEmitter.subscribe(status => {
      this.agentStatus = status
      this.predictable = this.isOneAgentsConnected(status)
    })

    //Listens for a socket from the backend. If it arrives, modals will open to show results
    this.getCall();
    this.agentPredicted();
  }

  //Made to disable the predict button if no agent is connected
  isOneAgentsConnected(agentStatuses){
    var result = false;
    for(var i of agentStatuses){
      result = result || i == 12;
    }
    return result;
  }

/*###################################################################################################
########## Prediction modal
###################################################################################################*/

  //Asks the backend for a prediction
  public predictBestAgent(){
    this.socketIoService.predictBestAgent('customer_satisfaction', this.sex, this.age, this.intent, this.year_of_arrival, this.salary);
  }

  //Processes the result send by the back-end to show it on the prediction modal
  public agentPredicted(): void {
    this.socketIoService.agentPredicted().subscribe(result => {
      //Gets the target objective and calculate the sum (for an average)
      this.targetValue = this.socketIoService.targetValue;
      var targetTotal = 0;
      for(var target of environment.targets){
        targetTotal += this.targetValue[target];
      }

      //resets the scores for each agent
      for(var agentNB in [0, 1, 2, 3, 4]){
        this.prediction[agentNB]['score'] = 0
      }
      
      //Sorts agents thanks to their agent number (from 1 to 5)
      this.prediction.sort((a,b) => {return a['agent_number'] < b['agent_number'] ? -1 : (a['agent_number'] == b['agent_number'] ? 0 : 1)});

      //Updates predictions and scores from the back-end's result, and rounds the score
      for(let target of environment.targets){
        for(var agentPrediction of result[target]){
          this.prediction[agentPrediction['agent_number'] - 1][target] = Math.round(agentPrediction['proba']*1000)/1000
          this.prediction[agentPrediction['agent_number'] - 1]['score'] += agentPrediction['proba']*this.targetValue[target]/targetTotal
        }
      }
      for(var agentNB in [0, 1, 2, 3, 4]){
        this.prediction[agentNB]['score'] = Math.round(this.prediction[agentNB]['score'] * 1000)/1000
      }

      //Sorts agents by score to get the best one one top
      this.prediction.sort((a,b) => {return a['score'] < b['score'] ? 1 : (a['score'] == b['score'] ? 0 : -1)});
      this.displayPredictionModal = true;
      setTimeout(() => {
        this.generateGraph();
      }, 1);
    })
  }

/*###################################################################################################
########## Call modal
###################################################################################################*/

  //Processes the result send by the back-end to show it on the call modal
  public getCall(): void {
    this.socketIoService.callArriving().subscribe(result => {
      this.intent = parseInt(result['customer']['intent']) - 1;
      this.age = result['customer']['customer_age'];
      this.sex = result['customer']['customer_gender'];
      this.year_of_arrival = result['customer']['customer_seniority'];
      this.salary = result['customer']['customer_salary'];
      this.agentInfo = {
        'name': result['agent']['agent_name'],
        'email': result['agent']['email'],
        'sex': result['agent']['agent_gender'],
        'age': result['agent']['agent_age'],
        'seniority': result['agent']['agent_seniority'],
      }
      this.satisfactionObservable = this.getSatisfaction();
      this.displayCallModal = true;
    })
  }

  //Listens at the back-end for the satisfaction
  getSatisfaction(): void {
    this.socketIoService.satisfactionArriving().subscribe(result => {
      console.log('satisfaction arriving: ' + result['rating'])
      this.displaySatisfaction = true;
      this.stars = [result['rating']>0, result['rating']>1, result['rating']>2, result['rating']>3, result['rating']>4];
    })
  }

/*###################################################################################################
########## Graph
###################################################################################################*/
  generateGraph(){
    var data = this.graphdataService.getGraphData(this.simulation.interactions, this.target)
    Plotly.newPlot('graphDiv2', data, {title: {text:"Past interactions used to train the routing algorithm:"}}, {responsive: true});
  }

/*###################################################################################################
########## Page mecanics
###################################################################################################*/  
  closeModals(){
    this.displayPredictionModal = false;
    this.displayCallModal = false;
    if(this.satisfactionObservable){
      this.satisfactionObservable.unsubscribe()
    }
    this.displaySatisfaction = false;
  }
}
