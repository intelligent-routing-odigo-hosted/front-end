import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Simulation } from 'src/app/models/Simulation';

import { SocketIoService } from 'src/app/services/socketIoService/socketio.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'simulation-picker',
  templateUrl: './simulation-picker.component.html',
  styleUrls: ['./simulation-picker.component.css', './bulma-tooltip.min.css']
})
export class SimulationPickerComponent implements OnInit {
  //Input and Output to the main app
  @Input() currentSimulation: Simulation;
  @Input() retrainMessage: string;
  @Output() upload = new EventEmitter();
  @Output() retrain = new EventEmitter();

  //Page mecanics
  targets: Object = {
    customer_satisfaction: 25,
    agent_satisfaction: 25,
    call_duration: 25,
    business_outcome: 25,
  }
  lastModifications: Array<string> = environment.targets;

  //Agent status
  agentStatus: Array<any>;

  constructor(private readonly socketIoService: SocketIoService) {
    //Refreshes agent status
    this.socketIoService.agentStatusEmitter.subscribe(status => {
      this.agentStatus = status;
    })

    this.socketIoService.sendTargetChange(this.targets);
  }
  
  ngOnInit() {}

  

/*###################################################################################################
########## Send information through outputs
###################################################################################################*/
  sendUploadOrder(){
    this.upload.emit(true);
  }

  sendRetrainOrder(){
    this.currentSimulation.isTrained = false;
    this.retrain.emit(true);
  }

/*###################################################################################################
########## Target changes
###################################################################################################*/
  sendTargetChange(target){
    //The sum of all target objectives can't be more than 100%, so when values are changed and the sum exceeds 100%, the last
    //target objective to have been modified is diminished
    var targetExcess = this.targets['customer_satisfaction'] + this.targets['agent_satisfaction'] + this.targets['call_duration'] + this.targets['business_outcome'] - 100;
    var index = this.lastModifications.indexOf(target);
    if(index != -1){
      this.lastModifications.splice(index, 1);
    }
    this.lastModifications.push(target)
    while(targetExcess > 0){
      if(targetExcess > this.targets[this.lastModifications[0]]){
        targetExcess -= this.targets[this.lastModifications[0]]
        this.targets[this.lastModifications[0]] = 0;
        this.lastModifications.push(this.lastModifications.shift())
      } else {
        this.targets[this.lastModifications[0]] -= targetExcess
        targetExcess = 0
      }
    }
    //sends the result to the back-end
    this.socketIoService.sendTargetChange(this.targets);
  }

/*###################################################################################################
########## Utils
###################################################################################################*/  
  public triggerDownload(){
    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(this.currentSimulation));
    var dlAnchorElem = document.getElementById('downloadAnchorElem');
    dlAnchorElem.setAttribute("href",     dataStr     );
    dlAnchorElem.setAttribute("download", "simulation.json");
    dlAnchorElem.click();
  }

  refreshAvailability(){
    this.socketIoService.askAgentsStatuses()
  }
}