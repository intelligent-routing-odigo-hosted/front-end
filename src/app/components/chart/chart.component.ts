import { Component, OnInit, Input, SimpleChange } from '@angular/core';
import Plotly from 'plotly.js-dist';
import { Simulation } from 'src/app/models/Simulation';
import { GraphdataService } from 'src/app/services/graphDataService/graphdata.service';

@Component({
  selector: 'chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  //Inputs from app
  @Input() simulation: Simulation;
  @Input() refresher: Number;

  //Page mecanics
  target: string = 'customer_satisfaction';

  //Options
  targetOptions: Object = [
    {id: 'customer_satisfaction', value: 'Customer satisfaction'},
    {id: 'agent_satisfaction', value: 'Agent satisfaction'},
    {id: 'call_duration', value: 'Agent efficiency'},
    {id: 'business_outcome', value: 'Business outcome'}
  ]

  constructor(private readonly graphdataService: GraphdataService) {}

  ngOnInit(){}

  ngOnChanges(changes: SimpleChange) {
    this.generateGraph();
  }

  generateGraph(){
    var data = this.graphdataService.getGraphData(this.simulation.interactions, this.target)
    Plotly.newPlot('graphDiv', data, {}, {responsive: true});
  }
}
