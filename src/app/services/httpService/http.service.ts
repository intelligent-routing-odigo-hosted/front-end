import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(private readonly http: HttpClient) { }

  getInteractionsFromZendesk(){
    return this.http.get(environment.backEndUrl + 'getInteractions')
  }

  getAgentData(){
    return this.http.get(environment.backEndUrl + 'agentInfo')
  }

  getZendeskCount(){
    return this.http.get(environment.backEndUrl + 'zendeskCount')
  }
}