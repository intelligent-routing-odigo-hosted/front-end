import { TestBed } from '@angular/core/testing';

import { GraphdataService } from './graphdata.service';

describe('GraphdataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GraphdataService = TestBed.get(GraphdataService);
    expect(service).toBeTruthy();
  });
});
