import { Injectable } from '@angular/core';
import { Interaction } from 'src/app/models/Interaction';

@Injectable({
  providedIn: 'root'
})
export class GraphdataService {
  getGraphData(interactions: Array<Interaction>, target: string): Object{
    var data = [{
      type: 'parcoords',
      line  : {
        showscale: true,
        color: this.unpack(interactions, target),
        colorscale: {
          customer_satisfaction: [[0, '#FF0000'], [0.6, '#FFFF00'], [0.66, '#FFFF00'], [1, '#00FF00']],
          agent_satisfaction: [[0, '#FF0000'], [0.6, '#FFFF00'], [0.66, '#FFFF00'], [1, '#00FF00']],
          call_duration: [[0, '#00FF00'], [0.3, '#FFFF00'], [0.35, '#FFFF00'], [1, '#FF0000']],
          business_outcome: [[0, '#FF0000'], [0.1, '#FFFF00'], [0.15, '#FFFF00'], [1, '#00FF00']]
        }[target]
      },
      dimensions: [{
        tickvals: [5,4,3,2,1,0],
        ticktext: ['Other', 'Appointment', 'Checks', 'New card', 'Loan', 'Card loss'],
        range: [0,5],
        label: 'Intent',
        values: this.unpack(interactions, 'intent')
      }, {
        tickvals: [0, 1, 2],
        ticktext: ['Male', 'Non Binary', 'Female'],
        constraintrange: [],
        range: [0,2],
        label: 'Customer gender',
        values: this.unpack(interactions, 'customer_gender')
      }, {
        range: [18,100],
        label: 'Customer age',
        values: this.unpack(interactions, 'customer_age')
      }, {
        range: [1970,2019],
        label: 'Customer since',
        values: this.unpack(interactions, 'customer_seniority')
      }, {
        tickvals: [0, 1, 2, 3, 4, 5],
        ticktext: ['< $10k', '$10k - $30k', '$30k - $50k', '$50k - $100k', '$100k - $200k', '> $200k'],
        constraintrange: [],
        range: [0,5],
        label: 'Customer salary',
        values: this.unpack(interactions, 'customer_salary')
      }, {
        tickvals: [1, 2, 3, 4, 5],
        ticktext: ['Agent 1', 'Agent 2', 'Agent 3', 'Agent 4', 'Agent 5'],
        range: [1, 5],
        label: 'Agent',
        values: this.unpack(interactions, 'agent_number')
      }, {
        range: {
          customer_satisfaction: [1,5],
          agent_satisfaction: [1, 5],
          call_duration: [0, 30],
          business_outcome: [0, 256]
        }[target],
        label: {
          customer_satisfaction: 'Customer satisfaction',
          agent_satisfaction: 'Agent satisfaction',
          call_duration: 'Agent efficiency',
          business_outcome: 'Business outcome'
        }[target],
        values: this.unpack(interactions, target)
      }]
    }];
    return data
  }

  //For satisfactions, there are only 5 possible values, it is thus displayed spread for us to see how much interactions there are
  //on the graph
  unpack(rows, key) {
    return rows.map(function(row) {
      if(key == 'customer_satisfaction' || key == 'agent_satisfaction'){
        if(row[key] == 1){
          return row[key] + Math.random()*0.25
        } else if (row[key] == 5){
          return row[key] - Math.random()*0.25
        } else {
          return row[key] + (Math.random() - 0.5)*0.5
        }
      } else {
        return row[key]; 
      }
    });
  }

  range(start, end) {
    return (new Array(end - start + 1)).fill(undefined).map((_, i) => i + start);
  }
}
