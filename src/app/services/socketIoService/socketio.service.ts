import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketIoService {
  private socket: SocketIOClient.Socket
  agentStatusEmitter: Subject<Array<any>> = new Subject<Array<any>>();
  targetValue: Object = {customer_satisfaction: 25, agent_satisfaction: 25, call_duration: 25, business_outcome: 25}

  constructor() {
    this.socket = io(environment.backEndUrl);
    this.getAgentStatuses();
  }

/*###################################################################################################
########## SocketIO - Zendesk interactions handling
###################################################################################################*/

  //Refreshing interactions before training
  refreshInteractions(): void{
    this.socket.emit('refreshInteractions');
  }

  refreshOver(){
    return new Observable<number>(observer => {
      this.socket.on('refreshOver', () => observer.next())
    })
  }

  //Uploads the interactions from the front-end to Zendesk (passing by the back-end)
  uploadInteractionsToZendesk(interactions){
    var json = {interactions: interactions};
    this.socket.emit('uploadInteractionsToZendesk', json);
  }

  onUploadOver(): Observable<Object> {
    return new Observable<Object>((observer) => {
      this.socket.on('onUploadOver', (result) => {
        observer.next(result);
      });
    });
  }

/*###################################################################################################
########## SocketIO - Model handling
###################################################################################################*/
  
  //Trains one model given its target
  train(target: string){
    this.socket.emit('train', {target: target});
  }

  onTrainingOver(){
    return  new Observable<Object>((observer) => {
      this.socket.on('trainingOver', (result) => observer.next(result['target']));
    });
  }

  //Used to get all agent scores for each of the 4 algorithms
  predictBestAgent(id: string, sex: number, age: number, skill: number, year_of_arrival: number, salary: number){
    var json = {"data": {
      "customer_gender": sex,
      "customer_age": age,
      "customer_salary": salary,
      "intent": skill,
      "customer_seniority": year_of_arrival
    }}
    this.socket.emit('predict_best_agent', json);
  }

  agentPredicted(): Observable<Array<any>> {
    return new Observable<Array<any>>((observer) => {
      this.socket.on('predict_best_agent_result', (result) => {
        observer.next(result.prediction);
      });
    });
  }

/*###################################################################################################
########## SocketIO - Event handling
###################################################################################################*/

  //When there is no interactions to query from Zendesk
  onEmptyZendesk(): Observable<Object> {
    return new Observable<Object>((observer) => {
      this.socket.on('noInteractions', () => observer.next());
    });
  }

  //When the back-end successfully inits
  onSuccessfulInit(): Observable<Object> {
    return new Observable<Object>((observer) => {
      this.socket.on('backEndInit', () => observer.next());
    });
  }

  //When a demoer calls the IVR, this is the function used to detect it and activate the call modal
  callArriving(): Observable<Object> {
    return new Observable<Object>((observer) => {
      this.socket.on('new_call', (result) => {
        observer.next(result);
      });
    });
  }

  //When a demoer ends the call, he can state his satisfaction to the IVR, this is the function used to detect it
  satisfactionArriving(): Observable<Object> {
    return new Observable<Object>((observer) => {
      this.socket.on('rating_added', (result) => {
        observer.next(result);
      });
    });
  }

/*###################################################################################################
########## SocketIO - Data handling
###################################################################################################*/

  //Broadcast the result of this socket route thanks to an event emitter
  getAgentStatuses() {
    this.socket.on('agentStatusResponse', (status) => {
      let formatedResult = []
      for(let agent of [1, 2, 3, 4, 5]){
        formatedResult.push(status["agentsStatuses"][agent])
      }
      this.agentStatusEmitter.next(formatedResult);
    });
  }

  askAgentsStatuses(): void{
    this.socket.emit('agentStatus', {emails: environment.agentsEmail});
  }

  //Sends the target objectives to the back end and saves it into the service for front-end broadcast
  sendTargetChange(json: Object): void{
    this.targetValue = json;
    this.socket.emit('targetChange', json)
  }
}