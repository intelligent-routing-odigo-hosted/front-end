import { Interaction } from './Interaction';

export class Simulation {
    interactions: Array<Interaction> = new Array<Interaction>();
    public isTrained: boolean;

    constructor( interactions: Array<any>, isTrained: boolean){
        if(interactions.length > 0){
            for (var interaction of interactions){
                this.interactions.push(new Interaction(
                    parseInt(interaction['agent_age']),
                    parseInt(interaction['agent_gender']),
                    parseInt(interaction['agent_number']),
                    parseInt(interaction['agent_satisfaction']),
                    parseInt(interaction['agent_seniority']),
                    parseInt(interaction['business_outcome']),
                    parseInt(interaction['call_duration']),
                    parseInt(interaction['customer_age']),
                    parseInt(interaction['customer_gender']),
                    parseInt(interaction['customer_salary']),
                    parseInt(interaction['customer_satisfaction']),
                    parseInt(interaction['customer_seniority']),
                    parseInt(interaction['intent']),
                ))}
        }
        this.isTrained = isTrained;
    }
}