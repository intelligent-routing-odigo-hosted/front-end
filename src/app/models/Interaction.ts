export class Interaction {
    agent_age: number;
    agent_gender: number;
    agent_number: number;
    agent_satisfaction: number;
    agent_seniority: number;
    business_outcome: number;
    call_duration: number;
    customer_age: number;
    customer_gender: number;
    customer_salary: number;
    customer_satisfaction: number;
    customer_seniority: number;
    intent: number;

    constructor(agent_age, agent_gender, agent_number, agent_satisfaction, agent_seniority, business_outcome, call_duration, customer_age, customer_gender, customer_salary, customer_satisfaction, customer_seniority, intent){
        this.agent_age = agent_age;
        this.agent_gender = agent_gender;
        this.agent_number = agent_number;
        this.agent_satisfaction = agent_satisfaction;
        this.agent_seniority = agent_seniority;
        this.business_outcome = business_outcome;
        this.call_duration = call_duration;
        this.customer_age = customer_age;
        this.customer_gender = customer_gender;
        this.customer_salary = customer_salary;
        this.customer_satisfaction = customer_satisfaction;
        this.customer_seniority = customer_seniority;
        this.intent = intent;
    }
}