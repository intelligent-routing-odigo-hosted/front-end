export const environment = {
  production: true,
  agentAvailabilityURL: 'https://routing-de01.prosodie.com/de01/servlet/SupervisionSnapshot?lg=WSOTH&pw=Prosodie1&ft=5&tb=4',
  agentsEmail: [
    'agent167ddi@demo.com',
    'agent167_1@demo.com',
    'agent167_2@demo.com',
    'agent167_3@demo.com',
    'agent167_4@demo.com',
  ],
  targets: ["customer_satisfaction", "agent_satisfaction", "call_duration", "business_outcome"],
  backEndUrl: 'http://demo5.prosodie.com/',
};
